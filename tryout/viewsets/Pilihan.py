from ..models.Pilihan import Pilihan as PilihanModel
from ..serializers.Pilihan import Pilihan as PilihanSerializer
from rest_framework.viewsets import  ModelViewSet
from ..filters.Pilihan import Pilihan as PilihanFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.status import HTTP_200_OK
from django.db import  transaction

class Pilihan(ModelViewSet):
    serializer_class = PilihanSerializer
    queryset = PilihanModel.objects.order_by('id',)
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class  = PilihanFilter
    permission_classes = [IsAuthenticated]