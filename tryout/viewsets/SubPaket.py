from ..models.SubPaket import SubPaket as SubPaketModel
from ..serializers.SubPaket import SubPaket as SubPaketSerializer
from rest_framework.viewsets import  ModelViewSet, GenericViewSet
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin, DestroyModelMixin
# from ..filters.SubPaket import SubPaket as SubPaketFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.status import HTTP_200_OK
from django.db import  transaction

class SubPaket(GenericViewSet, ListModelMixin, RetrieveModelMixin):
    serializer_class = SubPaketSerializer
    queryset = SubPaketModel.objects.order_by('id',)
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    # filterset_class  = SubPaketFilter
    permission_classes = [IsAuthenticated]
    
#     @transaction.atomic
#     def perform_create(self, serializer):
#         serializer.save(created_by = self.request.user.username)
#         GenerateNumberingSubPaket()
    
#     @transaction.atomic
#     def perform_update(self, serializer):
#         serializer.save(updated_by = self.request.user.username)
#         GenerateNumberingSubPaket()
        
#     @transaction.atomic
#     def perform_destroy(self, instance):
#         instance.delete()
#         GenerateNumberingSubPaket()

#     @action(detail=True)
#     def path(self, request, pk=None):
#         lst = []
#         parent = self.get_object()
#         lst.append(parent)
#         while(parent.parent != None):
#             lst.append(parent.parent)
#             parent = parent.parent
#         lst.reverse()
#         dataserializer = SubPaketSerializer(lst, many= True)
#         return Response(dataserializer.data, status=HTTP_200_OK)
    

# def GenerateNumberingSubPaket(child=None, number=None):
#     numb_inc = 1
#     childern = SubPaketModel.objects.filter(parent=child).order_by('id')
#     for child in childern:
#         child.number = (number or "") + str(numb_inc) + "."
#         child.save()
#         GenerateNumberingSubPaket(child, number=child.number)
#         numb_inc += 1