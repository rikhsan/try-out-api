from functools import partial
from django.contrib.auth.models import User as UserModel
from ..serializers.User import User as UserSerializer
from rest_framework.viewsets import  ModelViewSet, GenericViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from django.db import  transaction
from ..models.UsagePaket import UsagePaket as UsagePaketModel

class User(GenericViewSet, ListModelMixin, RetrieveModelMixin, UpdateModelMixin):
    serializer_class = UserSerializer
    queryset = UserModel.objects.order_by('-id')
    permission_classes = [IsAuthenticated]
