from ..models.UsagePaket import UsagePaket as UsagePaketModel
from ..serializers.UsagePaket import UsagePaket as UsagePaketSerializer, UsagePaketCreate as UsagePaketCreateSerializer
from rest_framework.viewsets import  ModelViewSet, GenericViewSet
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin, DestroyModelMixin
# from ..filters.UsagePaket import UsagePaket as UsagePaketFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.status import HTTP_200_OK
from django.db import  transaction
from ..serializers.UsageSubPaket import UsageSubPaket as UsageSubPaketSerializer
from ..models.UsageSubPaket import UsageSubPaket as UsageSubPaketModel
from django.utils import timezone

class UsagePaket(GenericViewSet, CreateModelMixin, ListModelMixin, RetrieveModelMixin, DestroyModelMixin):
    serializer_class = UsagePaketSerializer
    queryset = UsagePaketModel.objects.order_by('id',)
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    # filterset_class  = UsagePaketFilter
    permission_classes = [IsAuthenticated]
    
    
    def get_serializer_class(self):
        if self.action == 'create':
            return UsagePaketCreateSerializer
        return self.serializer_class
    
    def perform_create(self, serializer):
        # paket = PaketModel.objects.filter(id = serializer.validated_data['paket']).first()
        serializer.save(
            created_by = self.request.user.username, 
            user = self.request.user
            # finished_on = None, 
            # starter_on = None, 
            # durasi = serializer.validated_data['paket'].durasi
        )

    @action(detail=True, methods=['get'])
    def start(self, request, pk=None):
        if request.method == 'GET':
            usagePaket = self.get_object()
            if(not usagePaket.started_on):
                usagePaket.started_on = timezone.now()
                usagePaket.save()
            serializer = self.get_serializer(usagePaket, many=False)
            return Response(serializer.data, status=HTTP_200_OK)

    @action(detail=True, methods=['get'])
    def finish(self, request, pk=None):
        if request.method == 'GET':
            usagePaket = self.get_object()
            if(not usagePaket.finished_on):
                usagePaket.finished_on = timezone.now()
                usagePaket.save()
            serializer = self.get_serializer(usagePaket, many=False)
            return Response(serializer.data, status=HTTP_200_OK)
        
        # lst = []
        # parent = self.get_object()
        # lst.append(parent)
        # while(parent.parent != None):
        #     lst.append(parent.parent)
        #     parent = parent.parent
        # lst.reverse()
        # dataserializer = AkunSerializer(lst, many= True)
        # return Response(dataserializer.data, status=HTTP_200_OK)