from ..models.UsageSubPaket import UsageSubPaket as UsageSubPaketModel
from ..serializers.UsageSubPaket import UsageSubPaket as UsageSubPaketSerializer
from rest_framework.viewsets import  ModelViewSet, GenericViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin
from ..filters.UsageSubPaket import UsageSubPaket as UsageSubPaketFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.status import HTTP_200_OK
from django.db import  transaction

class UsageSubPaket(GenericViewSet, ListModelMixin, RetrieveModelMixin):
    serializer_class = UsageSubPaketSerializer
    queryset = UsageSubPaketModel.objects.order_by('id',)
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class  = UsageSubPaketFilter
    permission_classes = [IsAuthenticated]
    
#     @transaction.atomic
#     def perform_create(self, serializer):
#         serializer.save(created_by = self.request.user.username)
#         GenerateNumberingUsageSubPaket()
    
#     @transaction.atomic
#     def perform_update(self, serializer):
#         serializer.save(updated_by = self.request.user.username)
#         GenerateNumberingUsageSubPaket()
        
#     @transaction.atomic
#     def perform_destroy(self, instance):
#         instance.delete()
#         GenerateNumberingUsageSubPaket()

#     @action(detail=True)
#     def path(self, request, pk=None):
#         lst = []
#         parent = self.get_object()
#         lst.append(parent)
#         while(parent.parent != None):
#             lst.append(parent.parent)
#             parent = parent.parent
#         lst.reverse()
#         dataserializer = UsageSubPaketSerializer(lst, many= True)
#         return Response(dataserializer.data, status=HTTP_200_OK)
    

# def GenerateNumberingUsageSubPaket(child=None, number=None):
#     numb_inc = 1
#     childern = UsageSubPaketModel.objects.filter(parent=child).order_by('id')
#     for child in childern:
#         child.number = (number or "") + str(numb_inc) + "."
#         child.save()
#         GenerateNumberingUsageSubPaket(child, number=child.number)
#         numb_inc += 1