from ..models.UsageSoal import UsageSoal as UsageSoalModel
from ..serializers.UsageSoal import UsageSoal as UsageSoalSerializer
from rest_framework.viewsets import  ModelViewSet, GenericViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin
from ..filters.UsageSoal import UsageSoal as UsageSoalFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.status import HTTP_200_OK
from django.db import  transaction
from django.utils.timezone import now as TimeZoneNow

class UsageSoal(GenericViewSet, ListModelMixin, RetrieveModelMixin, UpdateModelMixin):
    serializer_class = UsageSoalSerializer
    queryset = UsageSoalModel.objects.order_by('id',)
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class  = UsageSoalFilter
    permission_classes = [IsAuthenticated]
    
    @transaction.atomic()
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        usage_paket = instance.usage_sub_paket.usage_paket
        if usage_paket.started_on == None:
            usage_paket.started_on = TimeZoneNow()
            usage_paket.save()
        return Response(serializer.data)
    
    
#     @transaction.atomic
#     def perform_create(self, serializer):
#         serializer.save(created_by = self.request.user.username)
#         GenerateNumberingUsageSoal()
    
#     @transaction.atomic
#     def perform_update(self, serializer):
#         serializer.save(updated_by = self.request.user.username)
#         GenerateNumberingUsageSoal()
        
#     @transaction.atomic
#     def perform_destroy(self, instance):
#         instance.delete()
#         GenerateNumberingUsageSoal()

#     @action(detail=True)
#     def path(self, request, pk=None):
#         lst = []
#         parent = self.get_object()
#         lst.append(parent)
#         while(parent.parent != None):
#             lst.append(parent.parent)
#             parent = parent.parent
#         lst.reverse()
#         dataserializer = UsageSoalSerializer(lst, many= True)
#         return Response(dataserializer.data, status=HTTP_200_OK)
    

# def GenerateNumberingUsageSoal(child=None, number=None):
#     numb_inc = 1
#     childern = UsageSoalModel.objects.filter(parent=child).order_by('id')
#     for child in childern:
#         child.number = (number or "") + str(numb_inc) + "."
#         child.save()
#         GenerateNumberingUsageSoal(child, number=child.number)
#         numb_inc += 1