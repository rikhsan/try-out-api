from django.contrib import admin

from .models.Paket import Paket as PaketModel
from .models.Pilihan import Pilihan as PilihanModel
from .models.Soal import Soal as SoalModel
from .models.SubPaket import SubPaket as SubPaketModel
from .models.UsageSubPaket import UsageSubPaket as UsageSubPaketModel
from .models.UsagePaket import UsagePaket as UsagePaketModel
from .models.UsageSoal import UsageSoal as UsageSoalModel
from django.contrib.admin import ModelAdmin

# class ItemPaket(ModelAdmin):
#     list_display = ("name", "is_active",)

# class SubPaketItem(ModelAdmin):
#     list_display = ("name", "is_active",)


admin.site.register(PaketModel)
admin.site.register(PilihanModel)
admin.site.register(SoalModel)
admin.site.register(SubPaketModel)