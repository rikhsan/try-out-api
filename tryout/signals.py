from django.db.models.signals import post_save, pre_delete, post_delete, pre_save, pre_delete
from django.dispatch import receiver
from .models.Paket import Paket as UsagePaketModel
from .models.UsagePaket import UsagePaket as UsagePaketModel
from .models.UsageSoal import UsageSoal as UsageSoalModel
from .models.UsageSubPaket import UsageSubPaket as UsageSubPaketModel
from .models.SubPaket import SubPaket as SubPaketModel
from .models.Soal import Soal as SoalModel



@receiver(post_save, sender=UsagePaketModel)
def usage_paket_post_save(sender, instance, created, **kwargs):
    if created:
        # instance.durasi = instance.paket.durasi
        # instance.started_on = None
        # instance.finished_on = None
        # instance.save()
        inc_number = 1
        for sub_paket in SubPaketModel.objects.filter(paket = instance.paket):
            usage_sp = UsageSubPaketModel.objects.create(sub_paket = sub_paket, usage_paket = instance)
            for soal in sub_paket.soal.all():
                UsageSoalModel.objects.create(usage_sub_paket = usage_sp, soal = soal, number = inc_number)
                inc_number += 1
        pass
