from __future__ import unicode_literals
from email.policy import default
from django.db.models import ForeignKey, TextField, PROTECT, DateTimeField, DecimalField, ManyToManyField, DateField, CharField
from django.forms import IntegerField
from django.utils import timezone
from .Base import BaseModel 
from django.core.exceptions import ValidationError
from django.db import  transaction
from .Soal import Soal as SoalModel
from .Paket import Paket as UsagePaketModel

class SubPaket(BaseModel):
    name = CharField(max_length=200, null=False, blank=False)
    deskripsi = TextField(null=False, blank=False)
    paket = ForeignKey(UsagePaketModel, on_delete=PROTECT, null=False, blank=False)
    soal = ManyToManyField(SoalModel, blank=True)
    passing_grade = DecimalField(decimal_places=0, max_digits=20, null=True, blank=True)
    
    class Meta:
        db_table = "try_out_sub_paket"
        verbose_name_plural = "Sub Paket"