from __future__ import unicode_literals
from email.policy import default
from django.db.models import ForeignKey, TextField, PROTECT, DateTimeField, DecimalField, ManyToManyField, DateField, CharField, IntegerField
from django.utils import timezone
from .Base import BaseModel 
from django.core.exceptions import ValidationError
from django.db import  transaction

class Paket(BaseModel):
    name = CharField(max_length=200, null=False, blank=False)
    deskripsi = TextField(null=False, blank=False)
    durasi = IntegerField(blank = True, null = True )
    
    class Meta:
        db_table = "try_out_paket"
        verbose_name_plural = "Paket"