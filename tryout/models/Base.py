from django.db.models import  CharField, DateTimeField, BooleanField, Model

class BaseModel(Model):
    is_active = BooleanField(blank=True, default=True)
    created_on = DateTimeField(auto_now_add=True, editable=False)
    updated_on = DateTimeField(auto_now=True, editable=False)
    created_by = CharField(max_length=150, default=None, null=True, editable=False)
    updated_by = CharField(max_length=150, default=None, null=True, editable=False)
    
    class Meta:
        abstract = True
        
