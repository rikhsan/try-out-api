from __future__ import unicode_literals
import numbers
from django.db.models import ForeignKey, PROTECT, IntegerField
from .Base import BaseModel 
from .Soal import Soal as SoalModel
from .Pilihan import Pilihan as PilihanModel
from .UsageSubPaket import UsageSubPaket as UsageSubPaketModel

class UsageSoal(BaseModel):
    usage_sub_paket = ForeignKey(UsageSubPaketModel, on_delete=PROTECT, null=False, blank=False, editable=False)
    soal = ForeignKey(SoalModel, on_delete=PROTECT, null=False, blank=False, editable=False)
    answer = ForeignKey(PilihanModel, on_delete=PROTECT, null=True, blank=True)
    number = IntegerField(blank=False, null=False, editable=False)
    
    class Meta:
        db_table = "try_out_usage_soal"
        verbose_name_plural = "Usage Soal"