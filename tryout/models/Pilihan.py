from __future__ import unicode_literals
from email.policy import default
from django.db.models import ForeignKey, TextField, PROTECT, DateTimeField, DecimalField, ManyToManyField, DateField
from django.forms import IntegerField
from django.utils import timezone
from .Base import BaseModel 
from django.core.exceptions import ValidationError
from django.db import  transaction
from .Soal import Soal as SoalModel
from ckeditor.fields import RichTextField

class Pilihan(BaseModel):
    soal = ForeignKey(SoalModel, on_delete=PROTECT, null=False, blank=False)
    pilihan = RichTextField(null=False, blank=False)
    value =DecimalField(decimal_places=0, max_digits=20, null=False, blank=False, default=0)
    
    class Meta:
        db_table = "try_out_pilihan"
        verbose_name_plural = "Pilihan"