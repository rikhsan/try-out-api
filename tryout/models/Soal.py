from __future__ import unicode_literals
from django.db.models import ForeignKey, TextField, PROTECT, DateTimeField, DecimalField, ManyToManyField, DateField
from django.utils import timezone
from .Base import BaseModel 
from django.core.exceptions import ValidationError
from django.db import  transaction
from ckeditor.fields import RichTextField

class Soal(BaseModel):
    soal = RichTextField(null=False, blank=False)
    pembahasan = RichTextField(null=True, blank=True)
    
    class Meta:
        db_table = "try_out_soal"
        verbose_name_plural = "Soal"