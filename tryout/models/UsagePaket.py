from __future__ import unicode_literals
from email.policy import default
from django.db.models import ForeignKey, TextField, PROTECT, DateTimeField, DecimalField, ManyToManyField, DateField, CharField, IntegerField
from django.utils import timezone
from .Base import BaseModel 
from django.core.exceptions import ValidationError
from django.db import  transaction
from .Soal import Soal as SoalModel
from django.contrib.auth.models import User as UserModel
from .Paket import Paket as UsagePaketModel

class UsagePaket(BaseModel):
    user = ForeignKey(UserModel, on_delete=PROTECT, null=False, blank=False, editable=False)
    paket = ForeignKey(UsagePaketModel, on_delete=PROTECT, null=False, blank=False)
    started_on = DateTimeField(null = True, blank = True, editable=False)
    finished_on = DateTimeField(null = True, blank = True, editable=False)
    keterangan = TextField(null=True, blank=True)
    
    class Meta:
        db_table = "try_out_usage_paket"
        verbose_name_plural = "Usage Paket"
        
    @transaction.atomic()
    def save(self, *args, **kwargs):
        super(UsagePaket, self).save(*args, **kwargs)