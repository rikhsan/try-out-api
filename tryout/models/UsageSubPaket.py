from __future__ import unicode_literals
from django.db.models import ForeignKey, PROTECT
from .Base import BaseModel 
from .UsagePaket import UsagePaket as UsagePaketModel
from .SubPaket import SubPaket as SubPaketModel

class UsageSubPaket(BaseModel):
    sub_paket = ForeignKey(SubPaketModel, on_delete=PROTECT, null=False, blank=False)
    usage_paket = ForeignKey(UsagePaketModel, on_delete=PROTECT, null=False, blank=False)
    
    class Meta:
        db_table = "try_out_usage_sub_paket"
        verbose_name_plural = "Usage Sub Paket"