    

from django.conf.urls import include
from django.urls import path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from .viewsets.Paket import Paket as PaketViewset
from .viewsets.Soal import Soal as SoalViewset
from .viewsets.Pilihan import Pilihan as PilihanViewset
from .viewsets.SubPaket import SubPaket as SubPaketViewset
from .viewsets.UsagePaket import UsagePaket as UsageViewset
from .viewsets.UsageSubPaket import UsageSubPaket as UsageSubPaketViewset
from .viewsets.UsageSoal import UsageSoal as UsageSoalViewset
# from .viewsets.BukuPembantu import BukuPembantu as BukuPembantuViewset
# from .viewsets.CashFlow import CashFlow as CashFlowViewSet
# from .viewsets.Piutang import Piutang as PiutangViewset
# from .viewsets.Hutang import Hutang as HutangViewset
# from .viewsets.AsetBarang import AsetBarang as AsetBarangViewSet
# from .viewsets.Image import Image as ImageViewset
# from .viewsets.Neraca import Neraca as NeracaViewset
from .viewsets.User import User as UserViewSet
# from .viewsets.Dashboard import Dashboard as DashboardViewSet

router = routers.DefaultRouter()
router.register('paket', PaketViewset, basename='paket')
router.register('soal', SoalViewset, basename='soal')
router.register('pilihan', PilihanViewset, basename='pilihan')
router.register('subpaket', SubPaketViewset, basename='subpaket')
router.register('usagepaket', UsageViewset, basename='usagepaket')
router.register('usagesubpaket', UsageSubPaketViewset, basename='usagesubpaket')
router.register('usagesoal', UsageSoalViewset, basename='usagesoal')
# router.register('bukupembantu', BukuPembantuViewset, basename='bukupembantu')
# router.register('cashflow', CashFlowViewSet, basename='cashflow')
# router.register('piutang', PiutangViewset, basename='piutang')
# router.register('hutang', HutangViewset, basename='hutang')
# router.register('asetbarang', AsetBarangViewSet, basename='asetbarang')
# router.register('image', ImageViewset, basename='image')
# router.register('neraca', NeracaViewset, basename='neraca')
router.register('user', UserViewSet, basename='user')
# router.register('dashboard', DashboardViewSet, basename='dashboard')

urlpatterns = [
    path('', include(router.urls)),
    path('auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]

