from django_filters import FilterSet, ModelChoiceFilter, CharFilter
import django_filters
from ..models.UsageSoal import UsageSoal as UsageSoalModel
from ..models.UsagePaket import UsagePaket as UsagePaketModel
from django.db.models import Q

class FieldFilterUsagePaket(ModelChoiceFilter):
    def filter(self, qs, value):
        if not value:
            return qs
        qs = qs.filter(usage_sub_paket__usage_paket=value)
        return qs

class UsageSoal(FilterSet):
    usage_paket = FieldFilterUsagePaket(label='Usage Paket', queryset=UsagePaketModel.objects.all())
    class Meta:
        model = UsageSoalModel
        fields = ('__all__')
        
