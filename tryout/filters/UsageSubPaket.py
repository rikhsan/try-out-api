from django_filters import FilterSet, ModelChoiceFilter, CharFilter
import django_filters
from ..models.UsageSubPaket import UsageSubPaket as UsageSubPaketModel

class UsageSubPaket(FilterSet):
    class Meta:
        model = UsageSubPaketModel
        fields = ('__all__')