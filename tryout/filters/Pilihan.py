from django_filters import FilterSet, ModelChoiceFilter, CharFilter
import django_filters
from ..models.Pilihan import Pilihan as PilihanModel
from ..models.UsagePaket import UsagePaket as UsagePaketModel
from ..models.UsageSoal import UsageSoal as UsageSoalModel

class FieldFilterUsagePaket(ModelChoiceFilter):
    def filter(self, qs, value):
        if not value:
            return qs
        lst_soal = [ us.soal for us in UsageSoalModel.objects.filter(usage_sub_paket__usage_paket = value)] 
        qs = qs.filter(soal__in = lst_soal)
        return qs

class Pilihan(FilterSet):
    usage_paket = FieldFilterUsagePaket(label='Usage Paket', queryset=UsagePaketModel.objects.all())
    class Meta:
        model = PilihanModel
        fields = ('__all__')
        
        

