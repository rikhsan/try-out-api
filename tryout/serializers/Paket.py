from ..models.Paket import Paket as UsagePaketModel
from rest_framework.serializers import ModelSerializer, SerializerMethodField, ValidationError

class Paket(ModelSerializer):
    class Meta:
        model = UsagePaketModel
        fields = ('__all__')