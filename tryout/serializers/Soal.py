from ..models.Soal import Soal as SoalModel
from rest_framework.serializers import ModelSerializer, SerializerMethodField, ValidationError

class Soal(ModelSerializer):
    class Meta:
        model = SoalModel
        fields = ('__all__')