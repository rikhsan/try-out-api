from ..models.UsageSubPaket import UsageSubPaket as UsageSubPaketModel
from rest_framework.serializers import ModelSerializer, SerializerMethodField, ValidationError
from ..serializers.SubPaket import SubPaket as SubPaketSerializer

class UsageSubPaket(ModelSerializer):
    sub_paket = SubPaketSerializer(read_only = True)
    
    class Meta:
        model = UsageSubPaketModel
        fields = ('__all__')