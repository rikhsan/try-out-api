from ..models.UsageSoal import UsageSoal as UsageSoalModel
from rest_framework.serializers import ModelSerializer, SerializerMethodField, ValidationError, DateTimeField
from ..serializers.Soal import Soal as SoalSerializer

class UsageSoal(ModelSerializer):
    soal = SoalSerializer(read_only = True)
    prev = SerializerMethodField(method_name='get_prev')
    next = SerializerMethodField(method_name='get_next')
    finished_on = DateTimeField(source='usage_sub_paket.usage_paket.finished_on', read_only=True)
    
    class Meta:
        model = UsageSoalModel
        fields = ('__all__')

    def get_prev(self, obj):
        prev = UsageSoalModel.objects.filter(usage_sub_paket__usage_paket = obj.usage_sub_paket.usage_paket, number = obj.number-1).first()
        return prev.id if prev else None
    
    def get_next(self, obj):
        next = UsageSoalModel.objects.filter(usage_sub_paket__usage_paket = obj.usage_sub_paket.usage_paket, number = obj.number+1).first()
        return next.id if next else None