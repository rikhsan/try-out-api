from ..models.UsagePaket import UsagePaket as UsagePaketModel
from rest_framework.serializers import ModelSerializer, SerializerMethodField, ValidationError
from .Paket import Paket as PaketSerializer
from ..models.UsageSoal import UsageSoal as UsageSoalModel

class UsagePaket(ModelSerializer):
    paket = PaketSerializer(read_only=True)
    start = SerializerMethodField(method_name='get_start')

    class Meta:
        model = UsagePaketModel
        fields = ('__all__')
    
    def get_start(self, obj):
        very_first = UsageSoalModel.objects.filter(usage_sub_paket__usage_paket__id = obj.id).order_by('number').first()
        return very_first.id if very_first else None
    
        
class UsagePaketCreate(ModelSerializer):
    class Meta:
        model = UsagePaketModel
        fields = ('__all__')
        
    def validate(self, attrs):
        usagepakets = UsagePaketModel.objects.filter(user = self.context['request'].user, finished_on__isnull = True)
        if usagepakets:
            raise ValidationError("Terdapat Try Out yang masih aktif")
