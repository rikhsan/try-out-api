from cProfile import label
from django.contrib.auth.models import User as UserModel
from rest_framework.serializers import ModelSerializer, ValidationError, Serializer, CharField
from django.conf import settings
from django.db import transaction
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate


class User(ModelSerializer):
    # profile = Profile(many=False)
    class Meta:
        model = UserModel
        fields = ('__all__')
        
