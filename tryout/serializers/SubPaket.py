from ..models.SubPaket import SubPaket as SubPaketModel
from rest_framework.serializers import ModelSerializer, SerializerMethodField, ValidationError

class SubPaket(ModelSerializer):
    class Meta:
        model = SubPaketModel
        fields = ('__all__')