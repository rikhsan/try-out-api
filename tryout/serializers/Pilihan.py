from ..models.Pilihan import Pilihan as PilihanModel
from rest_framework.serializers import ModelSerializer, SerializerMethodField, ValidationError

class Pilihan(ModelSerializer):
    class Meta:
        model = PilihanModel
        fields = ('__all__')